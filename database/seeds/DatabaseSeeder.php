<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\User;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        Model::unguard();

        DB::table('users')->delete();

        $users = array(
                ['username' => 'Driver1', 'password' => Bcrypt('driver1')],
                ['username' => 'Driver2', 'password' => Hash::make('driver2')],
                ['username' => 'Driver3', 'password' => Hash::make('driver3')],
        );
            
        // Loop through each user above and create the record for them in the database
        foreach ($users as $user)
        {
            User::create($user);
        }

        Model::reguard();
    }
}
