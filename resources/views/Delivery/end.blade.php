<!DOCTYPE html>
<html>
    <head>
        <title>End</title>
        <link rel="stylesheet" href="assets/css/style.css" />
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" />

    </head>
    <body>
        <div class="container">
           <h2>End Delivery</h2>
            <form class="form-horizontal" role="form" method="post" action="{{url('delivery/end/Cw0GfAEhlqBQBqZFQuDP5nixewTkB0zs4BNHkzXB')}}">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  <div class="form-group">
                        <label for="end_time" class="col-sm-2 control-label">End Time</label>
                        <div class="col-sm-4">
                          <!-- <input class="form-control" id="start_time" placeholder="Start Time" name="start_time"> -->
                          <input type="time" name="end_time" step="1" class="form-control" id="end_time" placeholder="End Time">
                        </div>
                  </div>
                  <div class="form-group">
                        <label for="end_reading" class="col-sm-2 control-label">End Reading</label>
                        <div class="col-sm-4">
                          <input class="form-control" id="end_reading" placeholder="End Reading"  name="end_reading">
                        </div>
                  </div>
                  <div class="form-group">
                        <label for="End_location" class="col-sm-2 control-label">End Location</label>
                        <div class="col-sm-4">
                          <input class="form-control" id="end_location" placeholder="End Location"  name="end_location">
                        </div>
                  </div>
                  <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                          <button type="submit" class="btn btn-success">Submit</button>
                        </div>
                  </div>
            </form>
    </div>
</body>
</html>
