<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableLoggedInDrivers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('logged_in_drivers', function (Blueprint $table) {
            $table->string('token')->unique();
            $table->timestamp('creation_date');
            $table->timestamp('expiry_date');
            $table->string('driver_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('logged_in_drivers');
    }
}
