<?php

use Illuminate\Database\Seeder;
use App\Delivery;


class DeliveriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $deliveries = array(
                ['driver_id' => 1,  'start_reading' => '2000', 'end_reading' => '5000','start_location' => 'lahore', 'end_location' => 'karachi'],
                ['driver_id' => 1,  'start_reading' => '6000', 'end_reading' => '8000','start_location' => 'islamabad', 'end_location' => 'murree'],
                ['driver_id' => 2,  'start_reading' => '3000', 'end_reading' => '8000','start_location' => 'lahore', 'end_location' => 'quetta'],
                
        );
            
        // Loop through each user above and create the record for them in the database
        foreach ($deliveries as $delivery)
        {
            Delivery::create($delivery);
        }

    }
}
