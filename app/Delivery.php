<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Delivery extends Model
{
    protected $fillable = ['driver_id', 'start_time', 'end_time','start_location','end_location','start_reading','end_reading'];
}
