<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Carbon\Carbon;

use App\User;
use App\LoggedInDriver;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //return a list of all users
        $users = User::all();
        return $users;
    }
    /*
     * Display login form
     */
    public function login( )
    {  
        return view ('users/login');
    }
    /*
     * see if the user is a valid user, return a token to signify a logged in user
     */
    public function validateUser(Request $request)
    {
        $username = $request->input('username');
        $password = $request->input('password');
        
        $user=User::where('username', '=', $username)->get()->first();
        if($user!= null && \Hash::check($password, $user->password))
        {
            //user is a driver, return a logged in token

            //create token object, store it
            $token=str_random(40); 
            $driver= new LoggedInDriver();
            $driver['token'] = $token;
            $driver['creation_date'] = Carbon::now();
            $driver['expiry_date'] = Carbon::now()->addDays(1);
            $driver['driver_id'] = $username;
            $driver->save();

            /*
             * return token in response 
             */
            return response()->json([
            'authenticated' => "true",
            'driver_id'=> $username, 
            'token' => $token,
            ]);
 
            /*$creation = Carbon::now();
            $expiry =  Carbon::now()->addDays(1);
            $now= Carbon::now()->addDays(3);

            if($now->diff($expiry)->days == 1) {
                $message="logged in";
            }
            else{
                $message="logged out";
            }*/         
          
        } 
        else{
            return response()->json([
            'authenticated' => "false", 
            'token' => null
            ]);
        }    
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
