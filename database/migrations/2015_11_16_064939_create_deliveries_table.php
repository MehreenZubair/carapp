<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeliveriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deliveries', function (Blueprint $table) {
            $table->increments('delivery_id');
            $table->string('driver_id');
            $table->timestamp('start_time');
            $table->timestamp('end_time');
            $table->integer('start_reading');
            $table->integer('end_reading');
            $table->string('start_location');
            $table->string('end_location');
            $table->timestamps();
        });

        Schema::table('deliveries', function($table) {
            $table->foreign('driver_id')->references('username')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
          Schema::drop('deliveries');
    }
   
}
