<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LoggedInDriver extends Model
{
    protected $fillable =['token','creation_date','expiry_date'];
}
