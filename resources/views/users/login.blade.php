<!DOCTYPE html>
<html>
    <head>
        <title>Driver Login</title>
        <link rel="stylesheet" href="assets/css/style.css" />
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" />

    </head>
    <body>
        <div class="container">
           <h2>Driver Login</h2>
            <form class="form-horizontal" role="form" method="post" action="{{url('users/login')}}">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  <div class="form-group">
                        <label for="username" class="col-sm-2 control-label">Username</label>
                        <div class="col-sm-4">
                          <input class="form-control" id="username" placeholder="Username" name="username">
                        </div>
                  </div>
                  <div class="form-group">
                        <label for="password" class="col-sm-2 control-label">Password</label>
                        <div class="col-sm-4">
                          <input class="form-control" id="password" placeholder="Password" type="Password" name="password">
                        </div>
                  </div>
                  <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                          <button type="submit" class="btn btn-success">Register</button>
                        </div>
                  </div>
            </form>
    </div>
</body>
</html>
