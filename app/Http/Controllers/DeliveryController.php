<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\LoggedInDriver;
use Carbon\Carbon;
use App\Delivery;


class DeliveryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($token)
    {
        // verify if the token is of a logged in user
        $driver = LoggedInDriver::where('token','=', $token)->get()->first();
        if($driver != null)
        {
            $tokenCreationDate= Carbon::createFromFormat('Y-m-d H:i:s',$driver->creation_date );
            if($tokenCreationDate->diff(Carbon::now())->days <= 1)
            {
                //user is logged in
                return view('delivery/start');
            }
            else
            {
                //user is logged out
                return response()->json([
                'message' => "Driver has to log in first"
                ]);
            }
        }
        else
        {
            return response()->json([
                'message' => "Invalid token"
                ]);
        }
        
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $token)
    {
        // verify if the token is of a logged in user
        $driver = LoggedInDriver::where('token','=', $token)->get()->first();
        if($driver != null)
        {
            $tokenCreationDate= Carbon::createFromFormat('Y-m-d H:i:s',$driver->creation_date );
            if($tokenCreationDate->diff(Carbon::now())->days <= 1)
            {
                //user is logged in
                $driverId=$driver->driver_id;
                $delivery= new Delivery();
                $delivery->driver_id = $driverId;
                $delivery->start_time = $request->start_time;
                $delivery->start_reading = $request->start_reading;
                $delivery->start_location = $request->start_location;
                $delivery->save();
                return response()->json([
                'message' => "Start Delivery Information for has been saved",
                ]);
            }
            else
            {
                //user is logged out
                return response()->json([
                'message' => "Driver has to log in first"
                ]);
            }
        }
        else
        {
            return response()->json([
                'message' => "Invalid token"
                ]);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for entering end delivery information.
     *
     * 
     * @return \Illuminate\Http\Response
     */

    public function edit($token)
    {
        // verify if the token is of a logged in user
        $driver = LoggedInDriver::where('token','=', $token)->get()->first();
        if($driver != null)
        {
            $tokenCreationDate= Carbon::createFromFormat('Y-m-d H:i:s',$driver->creation_date );
            if($tokenCreationDate->diff(Carbon::now())->days <= 1)
            {
                //user is logged in
                return view('delivery/end');
            }
            else
            {
                //user is logged out
                return response()->json([
                'message' => "Driver has to log in first"
                ]);
            }
        }
        else
        {
            return response()->json([
                'message' => "Invalid token"
                ]);
        }
  
    }

    /**
     * Update the end delivery information.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // verify if the token is of a logged in user
        $driver = LoggedInDriver::where('token','=', $token)->get()->first();
        if($driver != null)
        {
            $tokenCreationDate= Carbon::createFromFormat('Y-m-d H:i:s',$driver->creation_date );
            if($tokenCreationDate->diff(Carbon::now())->days <= 1)
            {
                //user is logged in
            
                
            }
            else
            {
                //user is logged out
                return response()->json([
                'message' => "Driver has to log in first"
                ]);
            }
        }
        else
        {
            return response()->json([
                'message' => "Invalid token"
                ]);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
