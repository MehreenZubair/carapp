<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/* show login form*/
Route::get('users/login', 'UserController@login');
/* Process login request, return token on successful login */
Route::post('users/login', 'UserController@validateUser');
Route::resource('users', 'UserController');

/*
 * Delivery routes
 */

/*Start a delivery, show form, after authenticating a logged in driver*/
Route::get('delivery/start/{token}', 'DeliveryController@create' );
/* validate driver from token, if valid, get delivery start data */
Route::post('delivery/start/{token}', 'DeliveryController@store' );
/*End a delivery, show form, after authenticating a logged in driver*/
Route::get('delivery/end/{token}', 'DeliveryController@edit' );
/* validate driver from token, if valid, get delivery end data */
Route::post('delivery/end/{token}', 'DeliveryController@update' );