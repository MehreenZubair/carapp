<!DOCTYPE html>
<html>
    <head>
        <title>Start</title>
        <link rel="stylesheet" href="assets/css/style.css" />
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" />

    </head>
    <body>
        <div class="container">
           <h2>Start Delivery</h2>
            <form class="form-horizontal" role="form" method="post" action="{{url('delivery/start/Cw0GfAEhlqBQBqZFQuDP5nixewTkB0zs4BNHkzXB')}}">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  <div class="form-group">
                        <label for="start_time" class="col-sm-2 control-label">Start Time</label>
                        <div class="col-sm-4">
                          <!-- <input class="form-control" id="start_time" placeholder="Start Time" name="start_time"> -->
                          <input type="time" name="start_time" step="1" class="form-control" id="start_time" placeholder="Start Time">
                        </div>
                  </div>
                  <div class="form-group">
                        <label for="start_reading" class="col-sm-2 control-label">Start Reading</label>
                        <div class="col-sm-4">
                          <input class="form-control" id="start_reading" placeholder="Start Reading"  name="start_reading">
                        </div>
                  </div>
                  <div class="form-group">
                        <label for="start_location" class="col-sm-2 control-label">Start Location</label>
                        <div class="col-sm-4">
                          <input class="form-control" id="start_location" placeholder="Start Location"  name="start_location">
                        </div>
                  </div>
                  <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                          <button type="submit" class="btn btn-success">Submit</button>
                        </div>
                  </div>
            </form>
    </div>
</body>
</html>
